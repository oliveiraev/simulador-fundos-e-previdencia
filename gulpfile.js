var gulp = require('gulp');
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    injectPartials = require('gulp-inject-partials'),
    useref = require('gulp-useref'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    uglify = require('gulp-uglify'),
    gulpIf = require('gulp-if'),
    del = require('del'),
    runSequence = require('run-sequence');
 
gulp.task('useref', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest('dist'))
});

gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
});

gulp.task('images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist/images'))
});

gulp.task('css', function() {
  return gulp.src('app/css/**/*')
  .pipe(gulp.dest('dist/css'))
});

gulp.task('index', function () {
  return gulp.src('app/index.html')
           .pipe(injectPartials())
           .pipe(gulp.dest('./dist'));
});

gulp.task('clean:dist', function() {
  return del.sync('dist');
});

gulp.task('sass', function(){
	return gulp.src('app/scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('browserSync', function(){
	browserSync.init({
		server: {
			baseDir: 'dist'
		},
	})
})

// WATCH
gulp.task('watch', ['browserSync', 'css','sass', 'index'], function(){
    	gulp.watch('app/scss/**/*.scss', ['sass']);
    	gulp.watch('app/css/**/*', ['css']);
    	gulp.watch('app/**/*.html', ['index']);
    	// Reloads the browser whenever HTML or JS files change
    	gulp.watch('./dist/*.html', browserSync.reload);  
    	gulp.watch('./dist/js/**/*.js', browserSync.reload); 
});

// BUILD
gulp.task('default', function (callback) {
	runSequence('clean:dist', 
    		['sass', 'useref', 'index', 'css', 'images', 'fonts', 'watch'],
    			callback
    		)
});